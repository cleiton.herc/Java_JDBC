import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	public static Connection getConnection() {
		try {
			//aqui é passado a Data Base, o user e a senha
			return DriverManager.getConnection("jdbc:mysql://localhost/escola","mastertech","Mastertech@123");
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}
}
