import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {
		
	public static void main(String[] args) throws SQLException {
		atualizar();
	}
	
	public static void deletar() {
		
		materiaDAO dao = new materiaDAO();
		dao.deletar("Ingles");
	}	
	
	public static void inserir() {
		
		// estancia a classe 
		Materia materia = new Materia();
					
		materia.setMateria("Ingless");
		materia.setAreaConhecimento("Read e Listening");
		
		materiaDAO dao = new materiaDAO();
		dao.inserir(materia);
	}
	
	public static void consultar() {
		
		materiaDAO dao = new materiaDAO();
		List<Materia> materias = dao.consultar();
		for(Materia materia: materias) {
			System.out.println("Materia: " + materia.getMateria() + " | Conhecimento: " + materia.getAreaConhecimento());
		}
	}
	
	public static void consultarMateria() {
		
		materiaDAO dao = new materiaDAO();
		Materia materia = dao.consultarMateria("Ingles");
		System.out.println("Materia: " + materia.getMateria() + " | Conhecimento: " + materia.getAreaConhecimento());
	}
	
	public static void atualizar() {
		
		materiaDAO dao = new materiaDAO();
		dao.atualizarConhecimento("Ingles", "Read Books");
	}
}
