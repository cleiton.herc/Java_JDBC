import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//classe para a conexao com o banco de dados ALUNO e tabela Materia
public class materiaDAO {
	
	private Connection conexao;
	
	//no construtor da classe, a conexao com a base ja esta sendo realizada
	public  materiaDAO() {
		// chama o metodo da classe Conexao para se conectar no banco de dados
		this.conexao = Conexao.getConnection();
	}
	
	public void inserir(Materia materia) {
		String query = "INSERT INTO materia (materia, areaConhecimento)" +
				       "VALUES (?,?)";
		    // cada ?, ? corresponde a uma coluna da tabela
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			//informar onde estao os valores das colunas
			//setString = campos String
			//setInt = campos inteiros
			// ...
			statement.setString(1, materia.getMateria());
			statement.setString(2, materia.getAreaConhecimento());
			
			// o metodo execute() efetiva o SQL no banco de dados
			statement.execute();
			
			// libera a conexao para criar um novo statement
			statement.close();
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public void deletar(String nomeMateria) {
		String query = "DELETE FROM materia " +
				       "WHERE materia = ?";
		    // cada ?, ? corresponde a uma coluna da tabela
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			//informar onde estao os valores das colunas
			//setString = campos String
			//setInt = campos inteiros
			// ...
			statement.setString(1, nomeMateria);
			
			// o metodo execute() efetiva o SQL no banco de dados
			statement.execute();
			
			// libera a conexao para criar um novo statement
			statement.close();
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public List<Materia> consultar() {
		
		String query = "SELECT * FROM materia";
		List<Materia> listaMaterias = new ArrayList<Materia>();
		    // cada ?, ? corresponde a uma coluna da tabela
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			// o metodo execute() efetiva o SQL no banco de dados e retorna o resultado da consulta no ResultSet
			ResultSet resultado = statement.executeQuery();
			
			while (resultado.next()) {
				Materia materia = new Materia();
				materia.setAreaConhecimento(resultado.getString("areaConhecimento"));
				materia.setMateria(resultado.getString("materia"));
				
				listaMaterias.add(materia);
	        }
			
			//fechando o ResultSet
			resultado.close();

			// libera a conexao para criar um novo statement
			statement.close();
			
			return listaMaterias;
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}

	public Materia consultarMateria(String nomeMateria) {
		
		String query = "SELECT * FROM materia WHERE materia = ?";
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, nomeMateria);
			
			// o metodo execute() efetiva o SQL no banco de dados e retorna o resultado da consulta no ResultSet
			ResultSet resultado = statement.executeQuery();
			
			Materia materia = new Materia();
			resultado.next();
			materia.setAreaConhecimento(resultado.getString("areaConhecimento"));
			materia.setMateria(resultado.getString("materia"));
				
			
			//fechando o ResultSet
			resultado.close();

			// libera a conexao para criar um novo statement
			statement.close();
			
			return materia;
		}
		catch (SQLException e){
			
			throw new RuntimeException(e);
		}
	}
	
	public void atualizarConhecimento(String nomeMateria, String conhecimento) {
		
		String query = "UPDATE materia set areaConhecimento = ? WHERE materia = ?";
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, conhecimento);
			statement.setString(2, nomeMateria);
			
			// o metodo execute() efetiva o SQL no banco de dados e retorna o resultado da consulta no ResultSet
			statement.execute();

			// libera a conexao para criar um novo statement
			statement.close();
			
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}

	
}
